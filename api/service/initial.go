package service

import (
	"gitlab.com/farkram/api_recipe/api/common"
	"gopkg.in/mgo.v2"
)

var (
	Session *mgo.Session
	db      *mgo.Database
)

// New service
func New() {

	common.StartUp()

	Session = common.GetSession().Copy()
}
